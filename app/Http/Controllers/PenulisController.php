<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Kategori;
use Illuminate\Support\Facades\Hash;
use App\Models\Komentar;


class PenulisController extends Controller
{
    public function index()
    {
        $totalTampilan = auth()->user()->penulis->post()->sum('tampilan');
        $totalPost = auth()->user()->penulis->post()->count();
        return view('penulis.dashboard_penulis', compact('totalTampilan', 'totalPost'));
    }

    public function simpanEdit(Request $request, $idpost)
    {
        //validasi
        $request->validate([
            'judul' => ['required'],
            'kategori' => ['required'],
            'isi' => ['required'],
            'gambar' => ['nullable', 'image'],
        ]);

        $post = Post::find($idpost);
        $post->judul = $request->judul;
        $post->idkategori = $request->kategori;
        $post->isipost = $request->isi;
        if ($request->gambar) {
            $post->file_gambar = $request->gambar->store('public/post');
        }

        $post->tgl_update = new \DateTime();


        $post->save();

        return redirect('penulis/post');
    }

    public function formTambah()
    {
        $kategori = Kategori::all();

        return view('penulis.tambah_postingan', compact('kategori'));
    }

    public function tambahPost(Request $request)
    {

        $request->validate([
            'judul' => ['required'],
            'kategori' => ['required'],
            'isi' => ['required'],
            'gambar' => ['required', 'image'],
        ]);


        $penulis = auth()->user()->penulis;

        $post = new Post();
        $post->judul = $request->judul;
        $post->idkategori = $request->kategori;
        $post->isipost = $request->isi;
        $post->file_gambar = $request->gambar->store('public/post');
        $post->tgl_insert = new \DateTime();
        $post->tgl_update = new \DateTime();

        $post->idpenulis = auth()->user()->penulis->idpenulis;
        $post->save();

        return redirect('penulis/post');
    }

    public function tampilFormEdit()
    {
        $user = auth()->user();
        $penulis = $user->penulis;

        return view('penulis.edit_profil', compact('user', 'penulis'));
    }

    public function simpan(Request $request)
    {
        $request->validate([
            'nama' => ['required', 'max:30'],
            'no_telp' => ['required', 'max:12'],
            'email' => ['required', 'email'],
            'password' => ['nullable'],
        ]);

        $user = auth()->user();
        $penulis = $user->penulis;
        $user->nama = $request->nama;
        $penulis->no_telp = $request->no_telp;
        $user->email = $request->email;
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        $penulis->alamat = $request->alamat;
        $penulis->kota = $request->kota;

        $penulis->save();
        $user->save();
        return back();
    }

    public function daftarPostingan()
    {
        $post = auth()->user()->penulis->post;

        return view('penulis.daftar_postingan', compact('post'));
    }

    public function hapus($idpost)
    {
        $post = Post::find($idpost);
        if ($post) {
            $post->delete();
        }

        return redirect('/penulis/post');
    }

    public function edit($idpost)
    {
        $post = Post::find($idpost);
        $kategori = Kategori::all();

        return view('penulis.edit_postingan', compact('post', 'kategori'));
    }

    public function hapusKomentar($idkomentar)
    {
        $komentar = Komentar::find($idkomentar);
        if ($komentar) {
            $komentar->delete();
        }
        return back();
    }
}
